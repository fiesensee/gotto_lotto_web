import React from 'react';
import Container from '@material-ui/core/Container'
import TextField from '@material-ui/core/TextField'
import './App.css';
import { Box, Button } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(theme => ({
  root: {
    '& > *': {
      margin: theme.spacing(1),
      // width: 100,
    },
    '& h2': {
      width: 200,
    },
  },
  input: {
    width: 75,
  }
}));

function App() {
  const classes = useStyles();
  return (
    <Box>
      <Container>
        <h1>Gotto Lotto</h1>
      </Container>
      <Container>
        <form className={classes.root}>
          <h2>Lotto Numbers</h2>
          <TextField className={classes.input} id="lotto-1" variant="outlined" />
          <TextField className={classes.input} id="lotto-2" variant="outlined" />
          <TextField className={classes.input} id="lotto-3" variant="outlined" />
          <TextField className={classes.input} id="lotto-4" variant="outlined" />
          <TextField className={classes.input} id="lotto-5" variant="outlined" />
          <TextField className={classes.input} id="lotto-6" variant="outlined" />
          <h2>Super Numbers</h2>
          <TextField className={classes.input} id="super-1" variant="outlined" />
          <TextField className={classes.input} id="super-2" variant="outlined" />
          <br/>
          <Button variant="contained" color="primary" size="large">Calculate Score</Button>
        </form>
      </Container>
    </Box>
  );
}

export default App;
